@isTest
public with sharing class Peskov_Info_Pronto_InsertTrigger_Test {
    @isTest static void TestInserProntoObject() {
        Opportunity opp = new Opportunity(Name='TestOpp', StageName='Contact Made', CloseDate=Date.valueOf('2019-12-31'));
        insert opp;

        Pricebook2 pb = new Pricebook2(Name='TestPriceBook');
        insert pb;

        Product2 product = new Product2(Name='TestProduct', CanUseQuantitySchedule=true);
        insert product;

        Id pricebookId = Test.getStandardPricebookId();
        PricebookEntry standardPBE = new PricebookEntry(Pricebook2Id = pricebookId, 
                                                        Product2Id = product.Id, 
                                                        UnitPrice = 1000, 
                                                        IsActive = true);
        insert standardPBE;

        PricebookEntry pbe = new PricebookEntry(Pricebook2=pb, 
                                                UnitPrice=888, 
                                                Pricebook2Id=pb.Id, 
                                                Product2Id=product.Id, 
                                                IsActive=true);
        insert pbe;

        OpportunityLineItem oppLineItem = 
            new OpportunityLineItem(OpportunityId=opp.Id, Quantity=1, TotalPrice=999, PricebookEntryId=pbe.Id);
        insert oppLineItem;

        OpportunityLineItemSchedule item = 
            new OpportunityLineItemSchedule(OpportunityLineItemId=oppLineItem.Id, Type='quantity', Quantity=1, ScheduleDate=Date.valueOf('2019-12-31'));
        insert item;

        Test.startTest();
        Pronto__c pronto = new Pronto__c(LineItemScheduleId__c=item.Id);
        insert pronto;
        Test.stopTest();

        OpportunityLineItemSchedule updatedScheduledItem = [SELECT Id, Pronto_Data__c FROM OpportunityLineItemSchedule WHERE Id = :item.Id];

        System.assertEquals(pronto.Id, updatedScheduledItem.Pronto_Data__c);
    }
}
