trigger Peskov_Info_Pronto_Insert on Pronto__c (after insert, after update) {
    for(Pronto__c pronto : Trigger.new) {
        OpportunityLineItemSchedule[] items = [SELECT Id, Pronto_Data__c  FROM OpportunityLineItemSchedule WHERE Id=:pronto.LineItemScheduleId__c];
        if (items.size() > 0) {
            OpportunityLineItemSchedule item = items[0];
            item.Pronto_Data__c = pronto.Id;

            update item;
        } else {
            //TODO: if no OppLineItem is found
        }
        
    }
}